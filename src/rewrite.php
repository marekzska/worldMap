<?php
// green, red, blue, yellow, orange
$colors = ["29D6BE", "D62941", "284FD7", "D7B028", "f28327", "a34bcc", "e632b9", "8a6132"];

$jsonString = file_get_contents('../public/resources/world.json');
$data = json_decode($jsonString, true);
foreach ($data as $key => $entry) {
    if ($entry['color'] !== 'gray') {
        echo $entry['title'];
        $entry['color'] = $colors[rand(0, 7)];
        echo $entry['color'] . PHP_EOL;
        $data[$key]['color'] = $colors[rand(0, 7)];
        echo $data[$key]['color'] . PHP_EOL;
    } else {
        $data[$key]['color'] = '335';
    }
}
$newJsonString = json_encode($data);
file_put_contents('world.json', $newJsonString);
