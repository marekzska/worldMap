import React, { useEffect, useState } from "react";
import "./Map.css";

export default function Map() {
  const [map, setMap] = useState([]);
  const [randomIterator, setRandomIterator] = useState();
  const [lookForClass, setlookForClass] = useState();
  const [score, setScore] = useState(0);
  const [isMoving, setIsMoving] = useState(false);
  const [viewBox, setViewBox] = useState({
    x: 0,
    y: -10,
    w: 1000,
    h: 690,
  });
  const [shuffledArray] = useState(
    [...Array(201).keys()].sort((a, b) => 0.5 - Math.random())
  );
  const [iterator, setIterator] = useState(0);
  const [offset, setOffset] = useState({ x: 0, y: 0 });

  useEffect(() => {
    fetch("./resources/world.json")
      .then((response) => response.json())
      .then((data) => {
        setMap(data);
      })
      .catch((error) => console.log(error));
  }, []);

  useEffect(() => {
    if (lookForClass !== "basic") {
      const timeOut = setTimeout(() => {
        setIterator((prev) => prev + 1);
      }, 1000);

      return () => {
        clearTimeout(timeOut);
      };
    }
  }, [lookForClass]);



  useEffect(() => {
    setlookForClass("basic");
    setRandomIterator(shuffledArray[iterator]);
  }, [iterator]);

  function handlePathClick(e) {
    console.log(e.target.getAttribute("title"));
    if (e.target.getAttribute("title") === map[randomIterator].title) {
      setlookForClass("correct");
      setScore((prev) => prev + 1);
    } else {
      setlookForClass("incorrect");
    }
    setViewBox({ x: 0, y: -10, w: 1000, h: 690 });
  }

  function getMousePosition(evt) {
    let CTM;
    if (evt.target.tagName === "svg") {
      CTM = evt.target.getScreenCTM();
    } else {
      CTM = evt.target.parentElement.getScreenCTM();
    }
    return {
      x: (evt.clientX + CTM.e) / CTM.a,
      y: (evt.clientY - CTM.f) / CTM.d,
    };
  }

  // function handleMouseDown(e) {
  //   if (!isMoving) {
  //     setOffset({
  //       x: getMousePosition(e).x,
  //       y: getMousePosition(e).y,
  //     });
  //     setIsMoving(true);
  //     console.log(getMousePosition(e));
  //   }
  // }

  useEffect(() => {
    console.log(viewBox);
  }, [viewBox]);

  // function handleMouseMove(e) {
  //   if (isMoving) {
  //     e.preventDefault();
  //     let coord = getMousePosition(e);
  //     console.log("offset " + offset.x + " " + offset.y);
  //     console.log("coord " + coord.x + " " + coord.y);
  //     console.log(" ");
  //     setViewBox({
  //       x: viewBox.x + (offset.x - coord.x) / 10,
  //       y: viewBox.y + (offset.y - coord.y) / 10,
  //       w: viewBox.w,
  //       h: viewBox.h,
  //     });
  //   }
  // }

  // function handleMouseUp(e) {
  //   setIsMoving(false);
  //   console.log(getMousePosition(e));
  // }

  function handleOnWheel(e) {
    let point;
    if (e.target.tagName === "svg") {
      point = e.target.createSVGPoint();
    } else {
      point = e.target.parentElement.createSVGPoint();
    }
    let normalized;
    let delta = e.wheelDelta;

    if (delta) {
      normalized = -(delta % 3 ? delta * 10 : delta / 3);
    } else {
      delta = e.deltaY || e.detail || 0;
      normalized = delta % 60 === 0 ? delta / 60 : delta / 12;
    }
    const scaleDelta = normalized > 0 ? 1 / 0.75 : 0.75;
    point.x = e.clientX;
    point.y = e.clientY;
    const startPoint = point.matrixTransform(e.target.getScreenCTM().inverse());

    if (
      viewBox.w * scaleDelta >= 20 &&
      viewBox.h * scaleDelta >= 0 &&
      viewBox.h * scaleDelta <= 690 &&
      viewBox.w * scaleDelta <= 1000
    ) {
      setViewBox({
        x: viewBox.x - (startPoint.x - viewBox.x) * (scaleDelta - 1),
        y: viewBox.y - (startPoint.y - viewBox.y) * (scaleDelta - 1),
        w: viewBox.w * scaleDelta,
        h: viewBox.h * scaleDelta,
      });
    } else if (normalized > 0 && viewBox.w === 1000) {
      setViewBox({
        x: 0,
        y: -10,
        w: 1000,
        h: 690,
      });
    }
  }

  return (
    <>
      {map.length > 0 && (
        <div className={lookForClass} id='lookFor'>
          {map[randomIterator].title}
        </div>
      )}
      {<div id='score'>Score: {score}</div>}
      <div id='svgContainer'>
        <svg
          xmlns='http://www.w3.org/2000/svg'
          // preserveAspectRatio="xMidYMid slice"
          // viewBox='0 -10 1000 670'
          viewBox={`${viewBox.x} ${viewBox.y} ${viewBox.w} ${viewBox.h}`}
          onWheel={handleOnWheel}
          // onMouseDown={handleMouseDown}
          // onMouseMove={handleMouseMove}
          // onMouseUp={handleMouseUp}
        >
          {map.map((item, index) => {
            return (
              <path
                fill={`#${item.color}`}
                key={index}
                onClick={handlePathClick}
                onWheel={handleOnWheel}
                // onMouseDown={handleMouseDown}
                // onMouseMove={handleMouseMove}
                // onMouseUp={handleMouseUp}
                d={item.coords}
                title={item.title}
                id={item.iso_code}
              />
            );
          })}
        </svg>
      </div>
    </>
  );
}
